package universite.angers.master.info.unknown.number.game.server;

import universite.angers.master.info.api.converter.Convertable;

/**
 * Classe qui permet de récupérer le nom de la commande dans un objet "ClientPlayer"
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerConverterCommandName implements Convertable<String, ServerPlayer> {

	@Override
	public String convert(ServerPlayer message) {
		return message.getCommand();
	}
}
