package universite.angers.master.info.unknown.number.game.server.command.request;

import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.unknown.number.game.server.ServerPlayer;
import universite.angers.master.info.unknown.number.game.server.ServerUnknownNumberGame;

/**
 * Classe qui permet de renvoyer l'état du jeu
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerRequestGame implements Commandable<ServerPlayer> {

	private ServerPlayer player;
	
	@Override
	public boolean send(ServerPlayer player) {
		//Si aucune instance du joueur
		if(player == null) return false;

		//On récupère le joueur coté serveur
		this.player = ServerUnknownNumberGame.getInstance().getOrNewPlayer(player.getIdentifiant());
		
		//On lui affiche l'état de la partie :
		//- L'interval du nombre recherché
		//- Les nombres d'essais restant pour chaque joueurs
		this.player.getMessages().add("Voici l'état de la partie : ");
		
		//L'interval
		String interval = "Le nombre inconnu est compris entre " + 
				ServerUnknownNumberGame.getInstance().getNumberRandomInf() + " et " +
				ServerUnknownNumberGame.getInstance().getNumberRandomSup();
		this.player.getMessages().add(interval);
		
		//Les nombres d'essais restant pour chaque joueurs
		for(ServerPlayer serverPlayer : ServerUnknownNumberGame.getInstance().getPlayers().values()) {
			String essai = "Il reste " + serverPlayer.getNumberOffer() + " essai(s) à " + serverPlayer.getName();
			this.player.getMessages().add(essai);
		}

		return true;
	}

	@Override
	public ServerPlayer receive(Object arg) {
		//On renvoit le joueur modifié dans la méthode "send"
		this.player.setCommand("REQUEST_GAME");
		return this.player;
	}
}
