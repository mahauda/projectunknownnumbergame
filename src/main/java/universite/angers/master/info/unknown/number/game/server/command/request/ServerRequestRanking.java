package universite.angers.master.info.unknown.number.game.server.command.request;

import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.unknown.number.game.server.ServerPlayer;
import universite.angers.master.info.unknown.number.game.server.ServerUnknownNumberGame;

/**
 * Classe qui permet de renvoyer le classement provisoire du jeu
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerRequestRanking implements Commandable<ServerPlayer> {

	private ServerPlayer player;
	
	@Override
	public boolean send(ServerPlayer player) {
		//Si aucune instance du joueur
		if(player == null) return false;

		//On récupère le joueur coté serveur
		this.player = ServerUnknownNumberGame.getInstance().getOrNewPlayer(player.getIdentifiant());
		
		//On lui affiche le classement serveur
		//- Le nombre de victoire de chaque joueur
		this.player.getMessages().add("Voici le classement du jeu : ");
		
		//Le nombre de victoire de chaque joueur
		for(ServerPlayer serverPlayer : ServerUnknownNumberGame.getInstance().getPlayers().values()) {
			String win = serverPlayer.getName() + " : " + serverPlayer.getNumberGameWin() + " victoire(s)";
			this.player.getMessages().add(win);
		}

		return true;
	}

	@Override
	public ServerPlayer receive(Object arg) {
		//On renvoit le joueur modifié dans la méthode "send"
		this.player.setCommand("REQUEST_RANKING");
		return this.player;
	}
}
