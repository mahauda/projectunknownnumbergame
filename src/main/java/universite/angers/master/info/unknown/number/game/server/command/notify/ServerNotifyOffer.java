package universite.angers.master.info.unknown.number.game.server.command.notify;

import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.unknown.number.game.server.ServerPlayer;
import universite.angers.master.info.unknown.number.game.server.ServerUnknownNumberGame;

/**
 * Classe qui permet d'afficher la proposition à tous les joueurs
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerNotifyOffer implements Commandable<ServerPlayer> {

	@Override
	public boolean send(ServerPlayer player) {
		System.out.println(player);
		return true;
	}

	@Override
	public ServerPlayer receive(Object arg) {
		ServerPlayer player = (ServerPlayer)arg;
		ServerPlayer playerMessage = new ServerPlayer();
		
		//On affiche l'offre du joueur
		String offer = player.getName() + " propose " + player.getNumberRamdomOffer() + ", ";
		
		boolean win = player.getNumberRamdomOffer() == ServerUnknownNumberGame.getInstance().getNumberRamdom();
		
		if(win) {
			//Si le nombre choisi aléatoirement par le joueur est celui du serveur dans ce cas gagné
			offer += "le nombre a été trouvé";
		} else {
			if(player.getNumberRamdomOffer() < ServerUnknownNumberGame.getInstance().getNumberRamdom()) {
				//Si le nombre proposé est plus petit dans ce cas on modifie l'intervalle inférieur
				//par le nombre proposé
				offer += "le nombre est plus grand";
				ServerUnknownNumberGame.getInstance().setNumberRandomInf(player.getNumberRamdomOffer());
			} else {
				//Si le nombre proposé est plus petit dans ce cas on modifie l'intervalle inférieur
				//par le nombre proposé
				offer += "le nombre est plus petit";
				ServerUnknownNumberGame.getInstance().setNumberRandomSup(player.getNumberRamdomOffer());
			}
		}
		
		playerMessage.getMessages().add(offer);
		
		//Si il n'y a plus de joueur pour faire une nouvelle proposition ou si gagné
		//dans ce cas on affiche la solution
		boolean playerCannotBeOffer = !ServerUnknownNumberGame.getInstance().playerCanBeOffer();
		if(win || playerCannotBeOffer) {
			String endGame = "Plus aucun joueur ne peut jouer, fin de partie";
			String soluceGame = "Le nombre inconnu était " + ServerUnknownNumberGame.getInstance().getNumberRamdom();
			playerMessage.getMessages().add(endGame);
			playerMessage.getMessages().add(soluceGame);
		}

		playerMessage.setCommand("NOTIFY_OFFER");
		return playerMessage;
	}
}
