package universite.angers.master.info.unknown.number.game.server.command.request;

import org.apache.log4j.Logger;

import universite.angers.master.info.network.communicator.Communicator;
import universite.angers.master.info.network.server.ServerService;
import universite.angers.master.info.network.server.Subject;
import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.unknown.number.game.server.ServerPlayer;
import universite.angers.master.info.unknown.number.game.server.ServerUnknownNumberGame;

/**
 * Classe qui permet de déconnecter un client
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerRequestClose implements Commandable<ServerPlayer>, Runnable {

	private static final Logger LOG = Logger.getLogger(ServerRequestClose.class);
	
	private ServerPlayer player;
	
	@Override
	public boolean send(ServerPlayer player) {
		//Si aucune instance du joueur
		if(player == null) return false;

		//On récupère le joueur coté serveur
		this.player = ServerUnknownNumberGame.getInstance().getOrNewPlayer(player.getIdentifiant());

		//On supprime le joueur
		ServerUnknownNumberGame.getInstance().removePlayer(this.player);
		
		//On lui affiche un message d'aurevoir
		String disconnect = "À bientôt " + this.player.getName();
		this.player.getMessages().add(disconnect);
		
		//Suppression coté serveur. On execute cette requete après une minute le temps que le serveur lui envoi le message
		new Thread(this, "RequestClose").start();
		
		return true;
	}

	@Override
	public ServerPlayer receive(Object arg) {
		//On renvoit le joueur modifié dans la méthode "send"
		this.player.setCommand("REQUEST_CLOSE");
		return this.player;
	}

	@Override
	public void run() {
		try {
			//On attend une minute avant d'éxécuter la requete pour supprimer la communication
			Thread.sleep(60000);
			
			//On supprime la com associé au joueur
			Communicator<?, ?> com = ServerUnknownNumberGame.getInstance().getServer().getCommunicatorServerServiceClients().remove(this.player.getIdentifiant());
			LOG.debug("Com to remove : " + com);
			
			if(com == null) return;
			
			//On la stoppe
			com.close();
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
