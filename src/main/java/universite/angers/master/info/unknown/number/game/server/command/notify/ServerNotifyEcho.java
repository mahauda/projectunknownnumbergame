package universite.angers.master.info.unknown.number.game.server.command.notify;

import universite.angers.master.info.network.server.Subject;
import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.unknown.number.game.server.ServerPlayer;

/**
 * Classe qui permet d'afficher une déconnection d'un client
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerNotifyEcho implements Commandable<ServerPlayer> {

	@Override
	public boolean send(ServerPlayer player) {
		System.out.println(player);
		return true;
	}

	@Override
	public ServerPlayer receive(Object arg) {
		ServerPlayer player = (ServerPlayer)arg;
		ServerPlayer playerMessage = new ServerPlayer();
		
		String disconnect = player.getName() + " es-tu toujours connecté ?";
		
		playerMessage.getMessages().add(disconnect);
		playerMessage.setCommand(Subject.NOTIFY_ECHO.getName());
		return playerMessage;
	}
}
