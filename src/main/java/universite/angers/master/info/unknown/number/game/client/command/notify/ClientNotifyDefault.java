package universite.angers.master.info.unknown.number.game.client.command.notify;

import universite.angers.master.info.network.server.Subject;
import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.unknown.number.game.client.ClientPlayer;

/**
 * Classe qui permet de recevoir n'importe quelle message du serveur
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ClientNotifyDefault implements Commandable<ClientPlayer> {

	private ClientPlayer player;
	
	public ClientNotifyDefault(ClientPlayer player) {
		this.player = player;
	}
	
	@Override
	public boolean send(ClientPlayer player) {
		for(String message : player.getMessages()) {
			System.out.println("["+this.player.getName()+"] " + message);
		}
		return true;
	}

	@Override
	public ClientPlayer receive(Object arg) {
		this.player.setCommand(Subject.NOTIFY_DEFAULT.getName());
		return this.player;
	}
}
